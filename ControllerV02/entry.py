from lib_V_02 import events as custom_events
from ryu.base import app_manager

class ControllerV02(app_manager.RyuApp):
    def __init__(self, *args, **kwargs):
        super(ControllerV02, self).__init__(*args, **kwargs)
        self.logger.info('Starting controller V02')

#Instantiate applications
app_manager.require_app('flow_manager.py')
app_manager.require_app('topology_discovery.py')
app_manager.require_app('traffic_manager.py')
app_manager.require_app('web_api.py')
app_manager.require_app('infra_monitor.py')

