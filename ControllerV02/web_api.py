#Events
from lib_V_02 import events as custom_events

from lib_V_02 import constants
#RYU
import json
from ryu.controller.handler import set_ev_cls
from ryu.base import app_manager
from ryu.app.wsgi import ControllerBase, WSGIApplication, route
from webob import Response
import networkx as nx

#Application that listens to events and keeps a copy of the data so it can serve it to the web controllers
class WebApi(app_manager.RyuApp):
    _CONTEXTS = {'wsgi': WSGIApplication}
    OFP_VERSION = constants.OFP_VERSION
    
    def __init__(self, *args, **kwargs):
        super(WebApi, self).__init__(*args, **kwargs)
        self.verbose = False
        self.tag = 'WEB_API: '

        self.topo_graph = {}
        self.hosts = {}

        #Register WSGI controllers:
        wsgi = kwargs['wsgi']
        print(str(wsgi))
        wsgi.register(InfoController, {'web_api_app': self})
    
        self.logger.info(self.tag + 'started, verbose=' + str(self.verbose))
    
    def _log(self, data):
        if self.verbose:
            self.logger.info(self.tag + ' ' + str(data))

    #Event listeners that gather data
    
    @set_ev_cls(custom_events.EVHostsChanged)
    def _hosts_update(self, ev):
        self.hosts = ev.hosts
        self._log('Hosts updated!')
        #self.logger.info(self.hosts)

    @set_ev_cls(custom_events.EVTopologyChanged)
    def _topology_update(self, ev):
        self.topo_graph = ev.topo_graph
        self._log('Topology updated!')


#Controllers for web APIs could be moved to a new file in the future if needed

class InfoController(ControllerBase):
    URL_TABLE = {
                'api_test': '/info/api_test/',
                'get_hosts': '/info/hosts/',
                'get_topo': '/info/topo/'
                }
    def __init__(self, req, link, data, **config):
        super(InfoController, self).__init__(req, link, data, **config)
        self.web_api_app = data['web_api_app']
        self._log('Info controller')
    
    def _log(self, data):
        if self.web_api_app.verbose:
            self.web_api_app._log(data)
    
    @route('info', URL_TABLE['api_test'], methods=['GET'])
    def api_test(self, req, **kwargs):
        body = json.dumps({'test_key': True})
        return Response(content_type='application/json', body=body.encode('UTF-8'))
    
    @route('info', URL_TABLE['get_hosts'], methods=['GET'])
    def get_hosts(self, reg, **kwargs):
        body = json.dumps(self.web_api_app.hosts)
        return Response(content_type='application/json', body=body.encode('UTF-8'))
    
    @route('info', URL_TABLE['get_topo'], methods=['GET'])
    def get_topo(self, reg, **kwargs):
        body = json.dumps(nx.readwrite.json_graph.cytoscape_data(self.web_api_app.topo_graph))
        #body = json.dumps("")
        return Response(content_type='application/json', body=body.encode('UTF-8'))
