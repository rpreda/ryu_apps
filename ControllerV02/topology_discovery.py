#Events
from lib_V_02 import events as custom_events
from lib_V_02.packet_utils import send_packet
from lib_V_02 import constants
#RYU
from ryu.base import app_manager
from ryu.topology.api import get_link, get_switch, get_all_host
from ryu.controller.handler import set_ev_cls
from ryu.app.ofctl.api import get_datapath
from ryu.topology import event as topo_event
from ryu.lib.packet import ethernet, ipv6, icmpv6, packet, ether_types
from ryu.lib import hub
#Extra libs
import networkx as nx


class TopologyDiscovery(app_manager.RyuApp):
    OFP_VERSION = constants.OFP_VERSION
    _EVENTS = [custom_events.EVTopologyChanged, custom_events.EVHostsChanged, custom_events.EVFloodActionsChanged,]

    def __init__(self, *args, **kwargs):
        super(TopologyDiscovery, self).__init__(*args, **kwargs)
        self.verbose = False
        self.topo_graph = None
        self.tag = 'TOPO_DISC: '
        #self.topology = {}
        self.flood_actions = {}
        self.trunk_delays = {}
        self.logger.info(self.tag + 'started, verbose=' + str(self.verbose))

    def _log(self, data):
        if self.verbose:
            self.logger.info(self.tag + ' ' + str(data))

    # Apart from topology we build here the list of flood_actions used for  
    # flooding a packet to the edge switches (switches which contain hosts)
    def _rebuild_topology(self):
        sw_list = get_switch(self, None)
        dpid_list = [sw.dp.id for sw in sw_list]
        self.flood_actions.clear()
        self.topo_graph = nx.DiGraph()
        self.topo_graph.add_nodes_from(dpid_list)
        for sw in sw_list:
            self.flood_actions.setdefault(sw.dp.id, []) 
            #Build list of all ports
            ports = [port.port_no for port in sw.ports]
            link_list = get_link(self, sw.dp.id)
            for lk in link_list:
                #Remove trunks from the port list, used for the flood actions, flood only on ports that are NOT a link source
                ports.remove(lk.src.port_no)
                # port': lk.src.port_no since we care about the port on the source, aka the current DPID from where we leave
                self.topo_graph.add_edge(sw.dp.id, lk.dst.dpid, mtr=constants.DEFAULT_MTR, ctr=constants.DEFAULT_CTR, delay=constants.DEFAULT_DELAY, out_port=lk.src.port_no)
            #Generate flood_actions for this switch , ONLY if it contains at least one port (host)
            if len(ports) > 0:
                datapath = get_datapath(self, sw.dp.id)
                parser = datapath.ofproto_parser
                self.flood_actions[sw.dp.id] = [parser.OFPActionOutput(out_port) for out_port in ports]
        # Send events to observers
        self.send_event_to_observers(custom_events.EVFloodActionsChanged(self.flood_actions))
        self.send_event_to_observers(custom_events.EVTopologyChanged(self.topo_graph))
        self._log('Topology information updated')

################## LISTENERS ##################

    @set_ev_cls(topo_event.EventLinkAdd)
    def _link_add(self, ev):
        self._rebuild_topology()
        self._log('Link added')
    
    @set_ev_cls(topo_event.EventLinkDelete)
    def _link_delete(self, ev):
        self._rebuild_topology()
        self._log('Link removed')

    @set_ev_cls(topo_event.EventSwitchEnter)
    def _switch_enter(self, ev):
        msg = ev.switch.to_dict()
        self._log('Switch dpid: ' + msg['dpid'] + ' entered!')
        self._rebuild_topology()
    
    @set_ev_cls(topo_event.EventSwitchLeave)
    def _switch_leave(self, ev):
        msg = ev.switch.to_dict()
        self._log('Switch dpid: ' + msg['dpid'] + ' left!')
        self._rebuild_topology()
