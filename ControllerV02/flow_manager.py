#Custom
from lib_V_02 import events as custom_events
from lib_V_02 import constants
#Ryu
from ryu.base import app_manager
from ryu.controller.handler import CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.app.ofctl.api import get_datapath
from ryu.controller import ofp_event
from ryu.lib import hub

class FlowManager(app_manager.RyuApp):
    OFP_VERSION = constants.OFP_VERSION

    def __init__(self, *args, **kwargs):
        super(FlowManager, self).__init__(*args, **kwargs)
        self.verbose = False
        self.tag = 'FLOW_MGR: '
        self.logger.info(self.tag + 'started, verbose=' + str(self.verbose))

    def _log(self, data):
        if self.verbose:
            self.logger.info(self.tag + ' ' + str(data))

#?   ################## Flow management functions, probably move to LIB ##################
    def add_flow(self, datapath, match, actions = [], instructions=[], priority=constants.DEFAULT_FLOW_PRIO, cookie=constants.DEFAULT_FLOW_COOKIE, hard_timeout=0, table_id=0):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions)]
        inst.extend(instructions)
        mod = parser.OFPFlowMod(datapath=datapath, 
                                cookie=cookie,
                                cookie_mask=0xFFFFFFFFFFFFFFFF, 
                                priority=priority, 
                                match=match, 
                                instructions=inst,
                                hard_timeout=int(hard_timeout),
                                table_id=table_id)
        datapath.send_msg(mod)

    def del_flow(self, datapath, match, table_id=0):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        mod = parser.OFPFlowMod(datapath=datapath,
                                command=ofproto.OFPFC_DELETE,
                                out_port=ofproto.OFPP_ANY,
                                out_group=ofproto.OFPG_ANY,
                                match=match,
                                table_id=table_id)
        datapath.send_msg(mod)

    def del_flow_by_cookie(self, datapath, cookie):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        mod = parser.OFPFlowMod(datapath=datapath_obj,
                                cookie=cookie,
                                cookie_mask=0xFFFFFFFFFFFFFFFF,
                                table_id=ofp.OFPTT_ALL,
                                command=ofp.OFPFC_DELETE,
                                out_port=ofp.OFPP_ANY,
                                out_group=ofp.OFPG_ANY)

    def ev_params(self, ev):
        return (ev.msg, ev.msg.datapath, ev.msg.datapath.ofproto, ev.msg.datapath.ofproto_parser)
    
#?   ################## LISTENERS ##################

#   Initial flows that should be installed on switches
    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _miss_table_entry(self, ev):
        (msg, datapath, ofproto, parser) = self.ev_params(ev)

        match = parser.OFPMatch()
        #Clear all flows present
        self.del_flow(datapath, match)
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER, ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath=datapath, priority=0, match=match, actions=actions, table_id=0)
        #Entry for probe packets sent during delay measrumetns
        #!match = parser.OFPMatch(eth_src=constants.PROBE_SRC_MAC, eth_dst=constants.PROBE_DST_MAC)
        match = parser.OFPMatch(eth_type=constants.PROBE_ETHER_TYPE)
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER, ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath=datapath, priority=1000, match=match, actions=actions)
        self._log('Initial entries added for: ' + str(datapath.id))

#* External flow alter events #
    @set_ev_cls(custom_events.EVFlowAdd)
    def _add_flow_handler(self, ev):
        self.add_flow(ev.datapath, ev.match, ev.actions, ev.instructions, ev.priority, ev.cookie, ev.hard_timeout, ev.table_id)
    
    @set_ev_cls(custom_events.EVDelDirectLink)
    def _del_direct_link_handler(self, ev):
        dp = get_datapath(self, ev.dpid)
        parser = dp.ofproto_parser
        match = parser.OFPMatch(eth_dst=ev.dst)
        self.del_flow(dp, match)
    
    @set_ev_cls(custom_events.EVFaultTrafficRedirect)
    def _del_port_flows(self, ev):
        dp = get_datapath(self, ev.dpid)
        parser = dp.ofproto_parser
        ofproto = dp.ofproto
        match = parser.OFPMatch()
        mod = parser.OFPFlowMod(datapath=dp,
                                command=ofproto.OFPFC_DELETE,
                                out_port=ev.port,
                                out_group=ofproto.OFPG_ANY,
                                match=match)
        dp.send_msg(mod)
