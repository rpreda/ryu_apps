import time
#Custom
from lib_V_02 import events as custom_events
from lib_V_02 import constants
from lib_V_02.packet_utils import send_packet
#RYU
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.topology.api import get_switch
from ryu.controller.handler import set_ev_cls
from ryu.controller.handler import MAIN_DISPATCHER, CONFIG_DISPATCHER
from ryu.app.ofctl.api import get_datapath
from ryu.lib import hub
from ryu.ofproto import ether
from ryu.lib.packet import ethernet, packet
from ryu.topology import event as topo_event
#Python extra
from networkx import get_edge_attributes


class InfraMonitor(app_manager.RyuApp):
    OFP_VERSION = constants.OFP_VERSION
    _EVENTS = [custom_events.EVFaultTrafficRedirect,]

    def __init__(self, *args, **kwargs):
        super(InfraMonitor, self).__init__(*args, **kwargs)

        self._pinging_thread = hub.spawn(self._ping_switches)
        self._measuring_thread = hub.spawn(self._measure_full_delay)
        self._link_measure_trhead = hub.spawn(self._measure_atr_ctr)
        self.verbose = False
        self.tag = 'INFRA_MON:'
        self.out_time_ctrl = {}
        #Use tuple (src_dpid, dst_dpid) as the key
        self.out_time_full = {}
        self.topo_graph = None
        self.switch_controller_delays = {}
        self.trunk_delays = {}
        #port_data = (src_dpid, dst_dpid): port
        self.port_data = {}
        self.past_traffic = {} #(dpid, port)->(total_bytes, time)
        self.logger.info(self.tag + 'started, verbose=' + str(self.verbose))
        
    def _log(self, data):
        if self.verbose:
            self.logger.info(self.tag + ' ' + str(data))

    def send_desc_stats_request(self, datapath):
        ofp_parser = datapath.ofproto_parser
        req = ofp_parser.OFPDescStatsRequest(datapath, 0)
        datapath.send_msg(req)

    def _measure_full_delay(self):
        while True:
            #Clear all the previously calculated delays
            hub.sleep(constants.LATENCY_DELAY)
            self.trunk_delays.clear()
            if self.topo_graph is None:
                continue
            for src_node in self.topo_graph.nodes:
                #Launch a probe packet on each trunk link
                out_edges = self.topo_graph.out_edges(src_node)
                #Out edge is a tuple representing the output edge
                for out_edge in out_edges:
                    data = self.topo_graph.get_edge_data(*out_edge)
                    self._send_probe(src_node, out_edge[1], data['out_port'])

    def _ping_switches(self):
        while True:
            sw_list = get_switch(self, None)
            for sw in sw_list:
                datapath = get_datapath(self, sw.dp.id)
                self.out_time_ctrl.setdefault(sw.dp.id, 0)
                self.switch_controller_delays.setdefault(sw.dp.id, constants.DEFAULT_DELAY)
                #! Time sensitive code
                self.send_desc_stats_request(datapath)
                tout = time.time()
                self.out_time_ctrl[sw.dp.id] = tout
                #! End time sensitive code
            hub.sleep(constants.LATENCY_DELAY)
            self._log('Switch ctrl delays: ' + str(self.switch_controller_delays))

    #*Assembles and sends RAW ethernet frames containing just the DPID of the switch
    def _send_probe(self, src_dpid, dst_dpid, port):
        e = ethernet.ethernet(src=constants.PROBE_SRC_MAC, dst=constants.PROBE_DST_MAC, ethertype=constants.PROBE_ETHER_TYPE)
        p = packet.Packet()
        p.add_protocol(e)
        p.add_protocol(str(src_dpid).encode('ASCII'))
        datapath = get_datapath(self, src_dpid)
        send_packet(datapath=datapath, port=port, pkt=p)
        self.out_time_full[(src_dpid, dst_dpid)] = time.time()

    def _measure_atr_ctr(self):
        while True:
            hub.sleep(constants.THROUGHPUT_DELAY)
            if self.topo_graph is None:
                continue
            #Launch OFPPortStats and OFPPortDesc for every node inside the infrastructure
            #We are measuring ONLY the outbound traffic, the inbound will be measured by another switch
            for node in self.topo_graph.nodes:
                dp = get_datapath(self, node)
                parser = dp.ofproto_parser
                ofproto = dp.ofproto
                req = parser.OFPPortDescStatsRequest(dp, 0) #Port desc for MTR (max transfer rate) aka curr speed
                dp.send_msg(req)#Get current port speed
                req = parser.OFPPortStatsRequest(dp, 0, ofproto.OFPP_ANY)#OFPPortStats to see how many bytes were sent, compute the delta and then get the rate knowing the delay
                dp.send_msg(req)

    #* We arrive here when there is a problem with the link starting from port on dpid
    #Remove flows directed to that port since a failiure/improper conditions occured, recalc needed
    def _link_fault_handler(self, dpid, port, reason='placeholder'):
        self.send_event_to_observers(custom_events.EVFaultTrafficRedirect(dpid, port))

    ####################EVENT LISTENERS####################

    #Handle the response to _ping_switches
    @set_ev_cls(ofp_event.EventOFPDescStatsReply, MAIN_DISPATCHER)
    def desc_stats_reply_handler(self, ev):
        t_arrive = time.time()
        dpid = ev.msg.datapath.id
        self.switch_controller_delays[dpid] = t_arrive - self.out_time_ctrl[dpid]

    @set_ev_cls(ofp_event.EventOFPPacketIn)
    def _packet_in_handler(self, ev):
        datapath = ev.msg.datapath
        e = packet.Packet(ev.msg.data).get_protocol(ethernet.ethernet)
        if e.ethertype == constants.PROBE_ETHER_TYPE:
            #Grab the arrival time before doing the processing
            arrival_time = time.time()
            pkt = packet.Packet(ev.msg.data)
            #Get the payload data as the last protocol present in the packets
            if len(pkt.protocols) > 1:
                #Remove padding characters and get the source DPID
                src_dpid = int(pkt.protocols[-1].decode('ASCII').replace('\x00', ''))
                if src_dpid in self.switch_controller_delays and datapath.id in self.switch_controller_delays:
                    #From the total delay subtract the delays
                    key = (src_dpid, datapath.id)
                    trunk_delay = round(arrival_time - self.out_time_full[key] - self.switch_controller_delays[src_dpid] - self.switch_controller_delays[datapath.id], 4)
                    #Sometimes the measured delay can be less than 0 due to the small processing overhead
                    if trunk_delay < 0:
                        trunk_delay = 0
                    self.trunk_delays[key] = trunk_delay
                    #self.send_event_to_observers(custom_events.EVDelayChanged(self.trunk_delays))
                    #! Moved here the delay update of the topo_graph
                    if key in self.topo_graph.edges:
                        self.topo_graph[key[0]][key[1]]['delay'] = trunk_delay
                    self._log('Trunk delays update: ' + str(self.trunk_delays))

    @set_ev_cls(custom_events.EVTopologyChanged)
    def _topology_update(self, ev):
        self.topo_graph = ev.topo_graph
        self._log('Topology updated!')
    
    #MTR/CTR Listeners
    #! SPEED IS IN KBPS !!!
    @set_ev_cls(ofp_event.EventOFPPortDescStatsReply, MAIN_DISPATCHER)
    def _port_desc_stats_reply_handler(self, ev):
        port_speed_map = {}
        dpid = ev.msg.datapath.id
        for port in ev.msg.body:
            port_speed_map[port.port_no] = port.curr_speed
        out_edges = self.topo_graph.out_edges(dpid)
        for edge in out_edges:
            self.topo_graph[edge[0]][edge[1]]['mtr'] = port_speed_map[edge[0]]
    
    @set_ev_cls(ofp_event.EventOFPPortStatsReply, MAIN_DISPATCHER)
    def port_stats_reply_handler(self, ev):
        port_bytes_map = {}
        dpid = ev.msg.datapath.id
        for port in ev.msg.body:
            #we care only about outbound traffic since we can only decide outbound not inbound
            port_bytes_map[port.port_no] = port.rx_bytes
        out_edges = self.topo_graph.out_edges(dpid)
        arrival = time.time()
        for edge in out_edges:
            self.past_traffic.setdefault((dpid, edge[0]), (0, arrival))
            #*traffic_delta/time_delta bytes/sec, to convert to kbs/sec => * 8/1000
            try:
                ctr = round((port_bytes_map[edge[0]] - self.past_traffic[(dpid, edge[0])][0]) / (arrival - self.past_traffic[(dpid, edge[0])][1]), 2)
            except ZeroDivisionError:
                ctr = 0
            self.topo_graph[edge[1]][edge[0]]['ctr'] = ctr
            #print('MTR: '+ str(self.topo_graph[edge[0]][edge[1]]['mtr']) + ' CTR: ' + str(ctr))
            self.past_traffic[(dpid, edge[0])] = (port_bytes_map[edge[0]], arrival)

    #Fault detection triggering flow deletion
    @set_ev_cls(topo_event.EventLinkDelete)
    def _link_delete(self, ev):
        self._link_fault_handler(ev.link.src.dpid, ev.link.src.port_no)
        
