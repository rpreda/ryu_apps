from ryu.ofproto import ofproto_v1_3, ofproto_v1_4

OFP_VERSION = [ofproto_v1_3.OFP_VERSION,]

#Cost function
COST_K0 = 10000000#(kbps measurements) => 1 for 1gbps
COST_K1 = 0.01# => 1 for 10 ms

# ? Topology discovery constants
SRC_EXCLUDE_MAC = '00:00:00:00:00:00'
ICMPv6_NXT_HEADER = 58
SOLICIT_INTERVAL = 5
DELAYS_UPDATE_INTERVAL = 10
DEFAULT_DELAY = 0 #100 ms initial assumed delay if measurments failed for some reason or before the initial measurements took place
DEFAULT_MTR = 1E7
DEFAULT_CTR = 0

# ? Infra monitor params
#Delay of latency measurements
LATENCY_DELAY = 1
PROBE_ETHER_TYPE = 52428 #0xCCCC
PROBE_SRC_MAC = '00:00:00:01:01:01' #SRC Mac address for the delay measurment probe packets
PROBE_DST_MAC = 'FF:FF:FF:FF:FF:FF'
#Delay of throughput measurements
THROUGHPUT_DELAY = 1


# ? Flow manager params/Traffic manager params
DEFAULT_FLOW_COOKIE = 0
DEFAULT_FLOW_PRIO = 1

FLOW_EXPIRE_MIN = 120 #1 minutes
FLOW_EXPIRE_DELTA = 12 #12 sec, random interval
FLOW_EXPIRE_GUARD = 1 #The flow entry on the first switch will expire first, thus stopping the traffic through the path before the handover to the new path
PATH_FLOW_PRIORITY = 1
