from ryu.controller.event import EventBase
from lib_V_02 import constants

class EVTopologyChanged(EventBase):
    def __init__(self, graph):
        super(EVTopologyChanged, self).__init__()
        #self.topology = topo
        self.topo_graph = graph

class EVHostsChanged(EventBase):
    def __init__(self, host_list):
        super(EVHostsChanged, self).__init__()
        self.hosts = host_list

class EVFloodActionsChanged(EventBase):
    def __init__(self, flood_actions):
        super(EVFloodActionsChanged, self).__init__()
        self.flood_actions = flood_actions

class EVDelayChanged(EventBase):
    def __init__(self, delays):
        super(EVDelayChanged, self).__init__()
        self.delays = delays

class EVFlowAdd(EventBase):
    def __init__(self, datapath, match, actions = [], instructions=[], priority=constants.DEFAULT_FLOW_PRIO, cookie=constants.DEFAULT_FLOW_COOKIE, hard_timeout=0, table_id=0):
        super(EVFlowAdd, self).__init__()
        self.datapath = datapath
        self.match = match
        self.actions = actions
        self.priority = priority
        self.cookie = cookie
        self.hard_timeout = hard_timeout
        self.instructions = instructions
        self.table_id = table_id

class EVDelDirectLink(EventBase):
    def __init__(self, dpid, dst):
        self.dpid = dpid
        self.dst = dst

class EVFaultTrafficRedirect(EventBase):
    def __init__(self, dpid, port):
        self.dpid = dpid
        self.port = port