#Events
from lib_V_02 import events as custom_events
from lib_V_02 import constants
#RYU
from ryu.base import app_manager
from ryu.lib import hub, mac
from ryu.controller.handler import set_ev_cls
from ryu.controller import ofp_event
from ryu.lib.packet import packet, ethernet, lldp
from ryu.topology.api import get_switch
from ryu.app.ofctl.api import get_datapath
#Extra LIBS
import _thread as thread
import time
import random
from networkx.algorithms.shortest_paths.weighted import dijkstra_path
from networkx.exception import NetworkXNoPath
from networkx import get_edge_attributes


class TrafficManager(app_manager.RyuApp):

    OFP_VERSION = constants.OFP_VERSION
    _EVENTS = [custom_events.EVFlowAdd, custom_events.EVDelDirectLink,]

    def __init__(self, *args, **kwargs):
        super(TrafficManager, self).__init__(*args, **kwargs)
        self.verbose = False
        self.flood_actions = {}
        self.topo_graph = None
        self.host_to_dpid_port = {}
        self.port_data = {}
        self.tag = 'TRAFFIC_MAN: '
        self.logger.info(self.tag + 'started, verbose=' + str(self.verbose))

    def _log(self, data):
        if self.verbose:
            self.logger.info(self.tag + ' ' + str(data))

    def _install_flow(self, eth_dst, dpid, out_port, expire):
        dp = get_datapath(self, dpid)
        parser = dp.ofproto_parser
        match = parser.OFPMatch(eth_dst=eth_dst)
        actions = [parser.OFPActionOutput(out_port, dp.ofproto.OFPCML_NO_BUFFER)]
        self.send_event_to_observers(custom_events.EVFlowAdd(dp, match, actions, priority=constants.PATH_FLOW_PRIORITY, hard_timeout=expire))

    def _packet_out(self, data, dpid, port=0, actions=None):
        dp = get_datapath(self, dpid)
        ofproto = dp.ofproto
        parser = dp.ofproto_parser

        actions = actions if actions is not None else [parser.OFPActionOutput(port, dp.ofproto.OFPCML_NO_BUFFER)]
        out = parser.OFPPacketOut(datapath=dp,
                                buffer_id=ofproto.OFP_NO_BUFFER,
                                actions=actions,
                                data=data,
                                in_port=ofproto.OFPP_CONTROLLER)
        dp.send_msg(out)

    def _is_from_switch(self, port, dpid):
        if dpid not in self.topo_graph.nodes:
            return True
        edges = self.topo_graph.out_edges(dpid)
        for edge in edges:
            #*If the input port is a port that connects to another switch return true
            if self.port_data[edge] == port:
                return True
        return False
            
    def _install_dst_direct_route(self, dpid, eth_dst, port):
        self._install_flow(eth_dst, dpid, port, 0)

    def cost_temp(self, s, d, edge_data):
        cost = constants.COST_K0/(edge_data['mtr'] - edge_data['ctr'] * 8) - 1 + edge_data['delay']/constants.COST_K1
        # print('SRC: ' + str(s) + ' DST: ' + str(d))
        # print('ATR factor: ' + str(constants.COST_K0/(edge_data['mtr'] - edge_data['ctr'] * 8)))
        # print('DEL factor: ' + str(edge_data['delay']/constants.COST_K1))
        # print('COST: ' + str(cost))
        # print()
        return cost
################## LISTENERS ##################
    #Switch the packet through the network
    @set_ev_cls(ofp_event.EventOFPPacketIn)
    def _packet_in_handler(self, ev):
        #Start thread for handling the packet that arrived
        thread.start_new_thread(self._packet_in_thread, (ev, ))

    def _packet_in_thread(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        ofproto, parser = datapath.ofproto, datapath.ofproto_parser
        pkt = packet.Packet(msg.data).get_protocol(ethernet.ethernet)
        # ! Ignore LLDP traffic so we don't mess the link discovery and ignore PROBE packets, or if the topology wasn't discovered yet
        if packet.Packet(msg.data).get_protocol(lldp.lldp) or pkt.ethertype == constants.PROBE_ETHER_TYPE or self.topo_graph is None:
            return
        dst = pkt.dst
        src = pkt.src
        in_port = msg.match['in_port']

        # Learn host if the packet is NOT coming from another switch
        if not self._is_from_switch(in_port, datapath.id):
            #* If host moved, delete old route
            if src in self.host_to_dpid_port and (datapath.id != self.host_to_dpid_port[src][0] or in_port != self.host_to_dpid_port[src][1]):
                self.send_event_to_observers(custom_events.EVDelDirectLink(datapath.id, dst))
            self.host_to_dpid_port[src] = (datapath.id, in_port) #! This is src dpid not current DPID since we should get packets to controller ONLY from src
        #Generate new path if we know the dst
        if dst in self.host_to_dpid_port:
            src_dpid = datapath.id  
            dst_dpid = self.host_to_dpid_port[dst][0]
            #*Operations on table 0
            if src_dpid == dst_dpid: #? Is the packet dst attached to this switch?
                #* Direct route should probably expire, or at least DELETE all other dst direct routes when changed
                self._install_flow(dst, src_dpid, self.host_to_dpid_port[dst][1], 0)
            else: #? Not intra switch, install the next leg of dijkstra
                #* Run dijkstra
                try:
                    path_raw = dijkstra_path(self.topo_graph, src_dpid, dst_dpid,
                        weight=self.cost_temp)
                        #lambda s, d, edge_data : constants.COST_K0/(edge_data['mtr'] - edge_data['ctr'] * 8) + edge_data['delay']/constants.COST_K1)
                except NetworkXNoPath:
                    self._log('ERROR: No path found, NON LINKED switches exist!')
                else:
                    expire = random.random() * constants.FLOW_EXPIRE_DELTA + constants.FLOW_EXPIRE_MIN
                    #* Make sure DST direct link path exists
                    self._install_dst_direct_route(dst_dpid, dst, port=self.host_to_dpid_port[dst][1])
                    for i in range(len(path_raw) - 1):
                        self._install_flow(dst, path_raw[i], self.topo_graph[path_raw[i]][path_raw[i+1]]['out_port'], expire)
            self._log('Route install')
            #?Forward packet to the destination instead of flooding it through the network
            self._packet_out(msg.data, dst_dpid, self.host_to_dpid_port[dst][1])
        else:
            #?Flood packet regardless if we don't know the DST
            for node in self.topo_graph.nodes:
                if node in self.flood_actions:
                    self._packet_out(msg.data, node, actions=self.flood_actions[node])

    @set_ev_cls(custom_events.EVTopologyChanged)
    def _topology_update(self, ev):
        self.topo_graph = ev.topo_graph
        self.port_data = get_edge_attributes(self.topo_graph, 'out_port')
        #self._log('Topology updated!')
    
    @set_ev_cls(custom_events.EVFloodActionsChanged)
    def _flood_actions_update(self, ev):
        self.flood_actions = ev.flood_actions
        #self._log('Flood actions updated!')