from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER, set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet, ethernet
from ryu.topology import event, switches
from ryu.topology.api import get_switch, get_link

class TestSwitch(app_manager.RyuApp):
    OFP_VERSION = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        #Call superclass constructor
        super(TestSwitch, self).__init__(*args, **kwargs)
        self.mac_to_port = {} #table for translating mac addresses to ports

    #Function to add flow to the switch
    def add_flow(self, datapath, priority, match, actions):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        #Construct flow modify message and send it to the switch
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions)]
        mod = parser.OFPFlowMod(datapath=datapath, priority=priority, match=match, instructions=inst)
        datapath.send_msg(mod)

    #Switch features is used as a 'moment in time' to register the 
    #table-miss flow entry, we don't use any of the features
    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        #Match everything
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                        ofproto.OFPCML_NO_BUFFER)]
        #Add an entry to the flowtable of the switch indicated by the datapath
        #priority 0 (lowest) match evreything and as an action send it to the
        #controller for further decisions
        self.logger.info('Adding default path to swtich: ' + str(datapath.id))
        self.add_flow(datapath, 0, match, actions)
    

    #Now hande the event in which a packet arrived and it matched the generic event
    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg = ev.msg
        #The switch
        datapath = msg.datapath
        #Openflow protocol used by the switch
        ofproto = datapath.ofproto
        #Parser used for this switch
        parser = datapath.ofproto_parser

        #Get the ID of the switch which sent the message
        dpid = datapath.id
        #Set the default entry in the mac to port table for this switch to an empty
        #dictonary s.t. we can add data to it
        self.mac_to_port.setdefault(dpid, {})
        

        #analyze the recieved packets with the packet library
        #by doing this we get back the source and destination MAC address
        pkt = packet.Packet(msg.data)
        eth_pkt = pkt.get_protocol(ethernet.ethernet)
        dst = eth_pkt.dst
        src = eth_pkt.src

        in_port = msg.match['in_port']
        #Tie the source address to the input port so that in the future
        #when we have src address as destination flood won't be necessary
        self.mac_to_port[dpid][src] = in_port
        #if we already know the port for dst then we can forward it to that port
        #otherwise we need to flood it
        #If destination has already been identified on this switch
        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD
        #action is to send the packet out, either to all ports or to the 
        #known port
        actions = [parser.OFPActionOutput(out_port)]

        #In the event that we know the destination and where this packet should arrive
        #we add the flow to the switch table
        if out_port != ofproto.OFPP_FLOOD:
            self.logger.info('Adding flow for in port: ' + str(in_port) + 
            ' and dst: ' + str(dst) + ' on switch: ' + str(dpid))
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
            self.add_flow(datapath, 1, match, actions)
        out = parser.OFPPacketOut(datapath=datapath,
                                buffer_id=ofproto.OFP_NO_BUFFER,
                                in_port=in_port, actions=actions,
                                data=msg.data)
        datapath.send_msg(out)