from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER, set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet, ethernet, lldp
from ryu.topology import event as topo_event
from ryu.topology import switches
from ryu.topology.api import get_link, get_switch
from ryu.lib import hub

from custom_events import EV_Request_Graph



app_manager.require_app('web_api.py')
class ControllerV01(app_manager.RyuApp):
    OFP_VERSION = [ofproto_v1_3.OFP_VERSION]
    _EVENTS = [EV_Request_Graph]
    def __init__(self, *args, **kwargs):
        super(ControllerV01, self).__init__(*args, **kwargs)
        #MAC to port on datapath table mac_to_port[datapath][addr]
        self.mac_to_port = {}
        self.monitor_thread = hub.spawn(self._monitor)
    ######################################### SWITCHING FUNCTIONS #########################################
    def _dumb_switch(self, msg, datapath, ofproto, parser):
        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})
        pkt = packet.Packet(msg.data).get_protocol(ethernet.ethernet)
        dst = pkt.dst
        src = pkt.src

        in_port = msg.match['in_port']
        self.mac_to_port[dpid][src] = in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD
        actions = [parser.OFPActionOutput(out_port)]
        if out_port != ofproto.OFPP_FLOOD:
            self.logger.info('Adding flow for in port: ' + str(in_port) + 
            ' and dst: ' + str(dst) + ' on switch: ' + str(dpid))
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
            self.add_flow(datapath, 1, match, actions)
        out = parser.OFPPacketOut(datapath=datapath,
                                buffer_id=ofproto.OFP_NO_BUFFER,
                                in_port=in_port, actions=actions,
                                data=msg.data)
        datapath.send_msg(out)


    ######################################### UTILITY FUNCTIONS #########################################
    def _print_links(self):
        links = get_link(self, None)
        link_port={(link.src.dpid, link.dst.dpid):link.src.port_no for link in links}
        self.info('Links: ' + str(link_port))
    
    def add_flow(self, datapath, priority, match, actions):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions)]
        mod = parser.OFPFlowMod(datapath=datapath, priority=priority, match=match, instructions=inst)
        datapath.send_msg(mod)
    
    def info(self, string):
        self.logger.info(string)
    
    def ev_params(self, ev):
        return (ev.msg, ev.msg.datapath, ev.msg.datapath.ofproto, ev.msg.datapath.ofproto_parser)
        

    def _monitor(self):
        while True:
            hub.sleep(10)
            self.info('Monitor thread tick')
            ev = EV_Request_Graph()
            #Create event
            self.send_event_to_observers(ev)
            
    ######################################### EVENT HANDLERS #########################################
    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _miss_table_entry(self, ev):
        (msg, datapath, ofproto, parser) = self.ev_params(ev)

        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER, ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath=datapath, priority=0, match=match, actions=actions)
        self.info('Default miss entry added, dpid: ' + str(datapath.id))
    
    #Handle packets coming from other hosts
    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        (msg, datapath, ofproto, parser) = self.ev_params(ev)
        
        #Ignore LLDP traffic so we don't mess the link discovery
        if packet.Packet(msg.data).get_protocol(lldp.lldp):
            return

        self._dumb_switch(msg, datapath, ofproto, parser)
        

    @set_ev_cls(topo_event.EventSwitchEnter)
    def _event_switch_enter_handler(self, ev):
        msg = ev.switch.to_dict()
        self.info('Switch with DPID: ' + str(msg['dpid']) + ' added!')

    @set_ev_cls(topo_event.EventSwitchLeave)
    def _event_switch_leave_handler(self, ev):
        msg = ev.switch.to_dict()
        self.info('Switch with DPID: ' + str(msg['dpid']) + ' removed!')

    @set_ev_cls(topo_event.EventHostAdd)
    def _event_host_add_handler(self, ev):
        msg = ev.host.to_dict()
        self.info('Host added ' + str(msg))