from ryu.base import app_manager
from ryu.controller.handler import set_ev_cls
from custom_events import EV_Request_Graph

class WebApi(app_manager.RyuApp):
    def __init__(self, *args, **kwargs):
        super(WebApi, self).__init__(*args, **kwargs)
    
    def info(self, string):
        self.logger.info(string)

    #will be a reversed logic
    @set_ev_cls(EV_Request_Graph)
    def _request_graph_handler(self, ev):
        self.info('Graph request called')
    